package pl.itsharks.sample.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import pl.itsharks.sample.repository.UserRepository;
import pl.itsharks.sample.repository.model.User;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    private static final String NAME = "Name1";
    private static final String PASSWORD = "Password1";
    private static final String HASHED_PASSWORD = "b9efd0093bc77cd96e6f86db09321ffe5cb4ffa7634df89df586379fe6276cc8";

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Before
    public void setUp() {
        User user = new User(NAME, HASHED_PASSWORD);

        Mockito.when(userRepository.save(ArgumentMatchers.any(User.class)))
                .thenReturn(user);
    }

    @Test
    public void userShouldBeSavedAndReturned() {
        User user = userService.saveUser(NAME, PASSWORD);

        assertEquals(HASHED_PASSWORD, user.getPassword());
        assertEquals(NAME, user.getName());
    }

}
