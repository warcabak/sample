package pl.itsharks.sample.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class SecurityHelperTest {

    @Test
    public final void shouldGenerateHashedValue() {
        String name = "Name1";
        String password = "Password1";
        String hashedPassword = "b9efd0093bc77cd96e6f86db09321ffe5cb4ffa7634df89df586379fe6276cc8";

        String hashedValue = SecurityHelper.hashPassword(name, password);

        assertEquals(hashedPassword, hashedValue);
    }

    @Test
    public final void shouldNotGenerateHashedValue() {
        String name = "Name1";
        String password = null;

        String hashedValue = SecurityHelper.hashPassword(name, password);

        assertNull(hashedValue);
    }

}
