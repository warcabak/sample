package pl.itsharks.sample;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;;
import pl.itsharks.sample.api.UserController;
import pl.itsharks.sample.api.dto.UserDTO;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserController userController;

    @Test
    public final void saveUser() {
        String name = "Name1";
        String password = "Password1";
        String hashedPassword = "b9efd0093bc77cd96e6f86db09321ffe5cb4ffa7634df89df586379fe6276cc8";

        UserDTO dto = userController.save(name, password);

        assertNotNull(dto);
        assertNotNull(dto.getName());
        assertNotNull(dto.getHashedPassword());
        assertEquals(dto.getHashedPassword(), hashedPassword);
    }

    @Test
    public void shouldReturnUserWithHashedPassword() throws Exception {

        mvc.perform(post("/api/users/Name1")
                .accept(MediaType.APPLICATION_JSON)
                .content("Password1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", Matchers.is("Name1")))
                .andExpect(jsonPath("$.hashedPassword", Matchers.is("b9efd0093bc77cd96e6f86db09321ffe5cb4ffa7634df89df586379fe6276cc8")));
    }

    @Test
    public void shouldReturnBadRequest() throws Exception {

        mvc.perform(post("/api/users/Name1Name1Name1Name1Name1")
                .accept(MediaType.APPLICATION_JSON)
                .content("Password1"))
                .andExpect(status().isBadRequest());
    }

}
