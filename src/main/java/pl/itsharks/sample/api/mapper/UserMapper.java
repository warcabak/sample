package pl.itsharks.sample.api.mapper;

import pl.itsharks.sample.api.dto.UserDTO;
import pl.itsharks.sample.repository.model.User;

public class UserMapper {

    public static UserDTO toDto(User user){
        UserDTO userDTO = new UserDTO();
        userDTO.setName(user.getName());
        userDTO.setHashedPassword(user.getPassword());
        return userDTO;
    }

}
