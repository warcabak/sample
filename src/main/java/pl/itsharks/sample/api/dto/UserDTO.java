package pl.itsharks.sample.api.dto;

import io.swagger.annotations.ApiModelProperty;

public class UserDTO {

    @ApiModelProperty(notes = "user name", required = true)
    private String name;

    @ApiModelProperty(notes = "users's hashed password", required = true)
    private String hashedPassword;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }
}
