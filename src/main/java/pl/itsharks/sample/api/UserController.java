package pl.itsharks.sample.api;

import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.itsharks.sample.api.dto.UserDTO;
import pl.itsharks.sample.api.mapper.UserMapper;
import pl.itsharks.sample.repository.model.User;
import pl.itsharks.sample.service.UserService;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@RestController
@RequestMapping("/api/users")
@Api(value = "/api/users", description = "REST exposure for USER resource")
@Validated
public class UserController {

    private final Logger log = LoggerFactory.getLogger(UserController.class);
    private static final String MIME_TYPE_V1 = "application/vnd.user.api.v1+json";

    @Autowired
    private UserService userService;

    @PostMapping(value = "/{name}", produces = {MIME_TYPE_V1, MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation("Save user details")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = UserDTO.class),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Internal server error")})
    public UserDTO save(@ApiParam(value = "User name", required = true) @NotBlank @Size(max = 20) @PathVariable String name,
                        @ApiParam(value = "User password", required = true) @NotBlank @RequestBody String password) {
        log.debug("REST request to save user : {}", name);
        User user = userService.saveUser(name, password);

        return UserMapper.toDto(user);
    }

}
