package pl.itsharks.sample.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.itsharks.sample.repository.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, String> {
}
