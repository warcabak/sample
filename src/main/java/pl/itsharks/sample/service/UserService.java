package pl.itsharks.sample.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.itsharks.sample.repository.UserRepository;
import pl.itsharks.sample.repository.model.User;
import pl.itsharks.sample.utils.SecurityHelper;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;
    
    public User saveUser(String name, String password) {
        String hashedPassword = SecurityHelper.hashPassword(name, password);
        User user = new User(name, hashedPassword);
        return userRepository.save(user);
    }
}
