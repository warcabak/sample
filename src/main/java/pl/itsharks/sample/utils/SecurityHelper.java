package pl.itsharks.sample.utils;

import org.apache.commons.codec.digest.DigestUtils;

public class SecurityHelper {

    public static String hashPassword(String name, String password) {
        if (name == null || password == null) {
            return null;
        }
        return DigestUtils.sha256Hex(name + password);
    }

}
