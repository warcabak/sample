# Sample application for saving user

## Start application
Application can be started using command:

    mvn spring-boot:run

## H2 Console
H2 web console can be accessed through:

<http://localhost:8080/h2-console/>

## API 
### Documentation
<http://localhost:8080/swagger-ui.html>

### Sample requests
In order to test working application you can use attached script files:

* Postman collection
    
    
    sample.postman_collection.json
    
* Intellij Idea http client

    
    sample.http
